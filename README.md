# Commands
- Add migration
    - `sqlx migrate add <name>`
- Migrate
    - `sqlx migrate run`

- Build css
    - `tailwindcss -o static/tailwind.css --minify`
