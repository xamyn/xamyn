-- Add migration script here
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE TABLE sessions(
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  user_id INTEGER NOT NULL REFERENCES users ON DELETE CASCADE,
  expires timestamptz NOT NULL DEFAULT timezone('utc', (now() + interval '3 day'))
);