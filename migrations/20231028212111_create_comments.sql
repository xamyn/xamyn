-- Add migration script here
-- create comments table
CREATE TABLE comments(
  id SERIAL PRIMARY KEY,
  user_id INTEGER NOT NULL REFERENCES users ON DELETE CASCADE,
  post_id INTEGER NOT NULL REFERENCES posts ON DELETE CASCADE,
  parent_comment_id INTEGER REFERENCES comments ON DELETE CASCADE,
  body TEXT NOT NULL,
  votes INTEGER DEFAULT 0,
  created timestamptz NOT NULL DEFAULT timezone('utc', now()),
  modified timestamptz NOT NULL DEFAULT timezone('utc', now())
);