-- Add migration script here
-- create user table
CREATE TABLE users(
  id SERIAL PRIMARY KEY,
  email VARCHAR(255) NOT NULL UNIQUE,
  password TEXT NOT NULL,
  username VARCHAR(127) NOT NULL UNIQUE,
  name VARCHAR(127) NOT NULL,
  created timestamptz NOT NULL DEFAULT timezone('utc', now()),
  modified timestamptz NOT NULL DEFAULT timezone('utc', now())
);
