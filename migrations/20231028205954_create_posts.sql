-- Add migration script here
-- create posts table
CREATE TABLE posts(
  id SERIAL PRIMARY KEY,
  votes INTEGER NOT NULL DEFAULT 0,
  title VARCHAR(255) NOT NULL,
  slug VARCHAR(255) NOT NULL,
  body TEXT NOT NULL,
  user_id INTEGER NOT NULL REFERENCES users ON DELETE CASCADE,
  topic_id INTEGER NOT NULL REFERENCES topics ON DELETE CASCADE,
  created timestamptz NOT NULL DEFAULT timezone('utc', now()),
  modified timestamptz NOT NULL DEFAULT timezone('utc', now())
);