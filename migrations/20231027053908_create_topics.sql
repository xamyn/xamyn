-- Add migration script here
-- create topics table
CREATE TABLE topics(
  id SERIAL PRIMARY KEY,
  name VARCHAR(127) NOT NULL UNIQUE,
  slug VARCHAR(127) NOT NULL UNIQUE,
  created timestamptz NOT NULL DEFAULT timezone('utc', now()),
  modified timestamptz NOT NULL DEFAULT timezone('utc', now())
);
