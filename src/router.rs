use crate::app::AppState;
use crate::middleware::auth_middleware;
use crate::routes::{create_post, create_user, fallback, get_post, index, login, topic_detail};
use axum::middleware;
use axum::routing::get;
use axum::Router;
use tower_http::services::ServeDir;
use tower_http::trace::TraceLayer;

pub async fn get_router() -> Router {
    let app_state = AppState::new().await;

    Router::new()
        .nest_service("/static/", ServeDir::new("static"))
        .route("/", get(index))
        .route("/users/create/", get(create_user).post(create_user))
        .route("/users/login/", get(login).post(login))
        .route("/t/:topic_slug/", get(topic_detail))
        .route("/t/:topic_slug/create/", get(create_post).post(create_post))
        .route("/p/:slug/", get(get_post))
        .fallback(fallback)
        .route_layer(middleware::from_fn_with_state(
            app_state.clone(),
            auth_middleware,
        ))
        .with_state(app_state)
        .layer(TraceLayer::new_for_http())
}
