use crate::db::get_db_pool;
use sqlx::postgres::Postgres;
use sqlx::Pool;
use tera::{Tera, Value, Result};
use markdown;
use std::collections::HashMap;


fn markdown_to_html_filter(val: &Value, _: &HashMap<String, Value>) -> Result<Value> {
    let html = markdown::to_html(val.as_str().unwrap().into());
    Ok(Value::String(html))
}


#[derive(Clone)]
pub struct AppState {
    pub tera: Tera,
    pub db: Pool<Postgres>,
}

impl AppState {
    pub async fn new() -> Self {
        let pool = get_db_pool().await;

        let mut tera = match Tera::new("templates/**/*.*ml") {
            Ok(t) => t,
            Err(e) => {
                println!("Parsing error(s): {}", e);
                ::std::process::exit(1);
            }
        };
        tera.register_filter("markdown_to_html", markdown_to_html_filter);
        AppState { tera, db: pool }
    }
}
