use crate::app::AppState;
use crate::models::Session;
use axum::http::header::HeaderMap;
use axum::{
    extract::{Request, State},
    middleware::Next,
    response::{IntoResponse, Redirect, Response},
};
use chrono::prelude::Utc;

pub async fn auth_middleware(
    State(state): State<AppState>,
    headers: HeaderMap,
    mut request: Request,
    next: Next,
) -> Response {
    let exempt_routes = ["/users/login/"];
    if exempt_routes.contains(&request.uri().path()) {
        return next.run(request).await;
    }
    let path = &request.uri().path().clone();
    if path.contains("/static/") {
        return next.run(request).await;
    }
    if !headers.contains_key("cookie") {
        return Redirect::to("/users/login/").into_response();
    }
    let cookies = match headers.get("cookie").unwrap().to_str() {
        Err(_) => return Redirect::to("/users/login/").into_response(),
        Ok(c) => c.to_string(),
    };
    let mut session_id = String::new();
    for cookie in cookies.split(";") {
        if cookie.contains("xamynSessionID") {
            session_id = cookie.replace("xamynSessionID=", "");
            session_id = session_id.replace(" ", "");
        }
    }
    let session = match Session::lookup(&state.db, "id".into(), session_id).await {
        Err(_) => return Redirect::to("/users/login/").into_response(),
        Ok(session) => {
            let now = Utc::now();
            if session.expires < now {
                return Redirect::to("/users/login/").into_response();
            }
            session
        }
    };
    request.extensions_mut().insert(session.user_id);
    let response = next.run(request).await;
    response
}
