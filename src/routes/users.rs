use crate::app::AppState;
use crate::models::Session;
use crate::models::User;
use argon2::{password_hash::PasswordHash, Argon2, PasswordVerifier};
use axum::extract::{rejection::FormRejection, State};
use axum::response::{Html, IntoResponse, Redirect};
use axum::{debug_handler, http::Method, Form};
use passwords::{analyzer, scorer};
use regex::Regex;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use tera::Context;

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct CreateUserForm {
    pub name: String,
    pub email: String,
    pub username: String,
    pub password: String,
    pub confirm_password: String,
}

impl CreateUserForm {
    fn new() -> Self {
        Self {
            name: String::new(),
            email: String::new(),
            username: String::new(),
            password: String::new(),
            confirm_password: String::new(),
        }
    }

    async fn validate(self, app: &AppState) -> Result<bool, HashMap<String, String>> {
        let email_regex = Regex::new(
            r"^([a-z0-9_+]([a-z0-9_+.]*[a-z0-9_+])?)@([a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,6})",
        )
        .unwrap();
        let username_regex = Regex::new(r"^[A-Za-z0-9]+(?:[ _-][A-Za-z0-9]+)*$").unwrap();
        let mut errors: HashMap<String, String> = HashMap::new();
        if self.name.is_empty() {
            errors.insert("name".into(), "A name is required".into());
        }
        if self.email.is_empty() {
            errors.insert("email".into(), "Email is required".into());
        } else if !email_regex.is_match(&self.email) {
            errors.insert("email".into(), "Not a valid email".into());
        } else {
            let query = User::lookup(&app.db, "email".into(), self.email.clone().into()).await;
            match query {
                Err(_) => {}
                Ok(_) => {
                    errors.insert(
                        "email".into(),
                        format!("{} has already been used", &self.email),
                    );
                }
            }
        }
        if self.username.is_empty() {
            errors.insert("username".into(), "A username is required".into());
        } else if !username_regex.is_match(&self.username) {
            errors.insert("username".into(), "Not a valid username".into());
        } else {
            let query =
                User::lookup(&app.db, "username".into(), self.username.clone().into()).await;
            match query {
                Err(_) => {}
                Ok(_) => {
                    errors.insert(
                        "username".into(),
                        format!("{} is already taken", &self.username),
                    );
                }
            }
        }
        if self.password.is_empty() {
            errors.insert("password".into(), "A password is required".into());
        } else {
            let analyzed_password = analyzer::analyze(self.password.clone());
            if analyzed_password.length() < 8 {
                errors.insert(
                    "password".into(),
                    "Must be at least 8 characters long".into(),
                );
            } else if analyzed_password.uppercase_letters_count() < 1 {
                errors.insert(
                    "password".into(),
                    "Must contain at least one uppercase letter".into(),
                );
            } else if analyzed_password.symbols_count() < 1 {
                errors.insert("password".into(), "Must contain at least one symbol".into());
            } else if analyzed_password.is_common() {
                errors.insert("password".into(), "This password is too common".into());
            } else if scorer::score(&analyzed_password) < 79.into() {
                errors.insert(
                    "password".into(),
                    "This password is not strong enough".into(),
                );
            } else if self.password != self.confirm_password {
                errors.insert("password".into(), "Passwords do not match".into());
                errors.insert("confirm_password".into(), "Passwords do not match".into());
            }
        }
        if errors.is_empty() {
            return Ok(true);
        }
        Err(errors)
    }
}

#[debug_handler]
pub async fn create_user(
    State(app): State<AppState>,
    method: Method,
    raw_form: Result<Form<CreateUserForm>, FormRejection>,
) -> Html<String> {
    let mut context = Context::new();
    let mut errors: HashMap<String, String> = HashMap::new();

    if method == "POST" {
        let form = match raw_form {
            Ok(f) => f,
            Err(_) => {
                context.insert("message", "Unknown error");
                return Html(app.tera.render("users/create_user.html", &context).unwrap());
            }
        };
        context.insert("form", &form.0);
        let user_form = form.0.clone();
        match form.0.validate(&app).await {
            Err(e) => errors = e,
            Ok(_) => {
                let user_res = User::create(
                    &app.db,
                    user_form.email.clone(),
                    user_form.password.clone(),
                    user_form.username.clone(),
                    user_form.name.clone(),
                )
                .await;
                println!("user res is {:?}", user_res);
            }
        }
    } else {
        let new_form = CreateUserForm::new();
        context.insert("form", &new_form);
    }
    context.insert("errors", &errors);
    Html(app.tera.render("users/create_user.html", &context).unwrap())
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct LoginForm {
    pub username: String,
    pub password: String,
}

impl LoginForm {
    fn new() -> Self {
        Self {
            username: String::new(),
            password: String::new(),
        }
    }
}

pub async fn login(
    State(app): State<AppState>,
    method: Method,
    raw_form: Result<Form<LoginForm>, FormRejection>,
) -> axum::response::Response {
    let mut context = Context::new();
    if method == "POST" {
        let form = match raw_form {
            Ok(f) => f,
            Err(_) => {
                context.insert("message", "Unknown error");
                return Html(app.tera.render("users/login.html", &context).unwrap())
                    .into_response();
            }
        };
        let query = User::lookup(&app.db, "username".into(), form.username.clone().into()).await;
        if query.is_ok() {
            let user = query.unwrap();
            let password = user.password.clone();
            let parsed_hash = PasswordHash::new(&password.value).unwrap();
            if Argon2::default()
                .verify_password(&form.password.clone().into_bytes(), &parsed_hash)
                .is_ok()
            {
                let session = Session::create(&app.db, user.id.clone()).await.unwrap();
                let mut res = Redirect::to("/").into_response();
                let expires = session.expires.format("%a, %d %b %Y %T GMT");
                let cookie = format!(
                    "xamynSessionID={}; SameSite=Strict; Expires={}; Path=/",
                    session.id, expires
                );
                res.headers_mut()
                    .insert("Set-Cookie", cookie.parse().unwrap());
                return res;
            }
        }
        context.insert("form", &form.0);
        context.insert("message", "Invalid credentials");
        return Html(app.tera.render("users/login.html", &context).unwrap()).into_response();
    }
    let form = LoginForm::new();
    context.insert("form", &form);
    Html(app.tera.render("users/login.html", &context).unwrap()).into_response()
}
