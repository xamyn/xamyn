use crate::app::AppState;
use axum::extract::{Path, State};
use axum::http::StatusCode;
use axum::response::Html;
use tera::Context;

pub async fn fallback(State(app): State<AppState>) -> Html<String> {
    let context = Context::new();
    Html(app.tera.render("404.html", &context).unwrap())
}
