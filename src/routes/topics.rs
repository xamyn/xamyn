use crate::app::AppState;
use crate::models::{Post, Topic, User};
use axum::extract::{Path, State};
use axum::response::Html;
use tera::Context;

pub async fn topic_detail(State(app): State<AppState>, topic_slug: Path<String>) -> Html<String> {
    println!("topic {:?}", topic_slug.0);
    let mut context = Context::new();

    let topic = match Topic::get(&app.db, topic_slug.0).await {
        Err(_) => return Html(app.tera.render("404.html", &context).unwrap()),
        Ok(r) => match r {
            None => return Html(app.tera.render("404.html", &context).unwrap()),
            Some(t) => t,
        },
    };

    match Post::by_topic(&app.db, topic.id).await {
        Err(_) => println!("error"),
        Ok(posts) => {
            let post = posts.first().unwrap();
            let user = User::lookup(&app.db, "id".into(), format!("{}", post.user_id)).await.expect("Missing user");
            let topic = Topic::lookup(&app.db, "id".into(), format!("{}", post.topic_id)).await.expect("Missing topic");
            context.insert("posts", &posts);
            context.insert("post", &post);
            context.insert("topic", &topic);
            context.insert("user", &user);
        }
    }
    Html(app.tera.render("index.html", &context).unwrap())
}
