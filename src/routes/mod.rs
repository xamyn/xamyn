mod fallback;
mod index;
mod posts;
mod topics;
mod users;

pub use fallback::fallback;
pub use index::index;
pub use posts::{create_post, get_post};
pub use topics::topic_detail;
pub use users::{create_user, login};
