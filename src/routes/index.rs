use crate::app::AppState;
use crate::models::{Post, User, Topic};
use axum::extract::State;
use axum::response::Html;
use tera::Context;

pub async fn index(State(app): State<AppState>) -> Html<String> {
    let mut context = Context::new();
    match Post::all(&app.db).await {
        Err(_) => println!("error"),
        Ok(posts) => {
            let post = posts.first().unwrap();
            let user = User::lookup(&app.db, "id".into(), format!("{}", post.user_id)).await.expect("Missing user");
            let topic = Topic::lookup(&app.db, "id".into(), format!("{}", post.topic_id)).await.expect("Missing topic");
            context.insert("posts", &posts);
            context.insert("post", &post);
            context.insert("topic", &topic);
            context.insert("user", &user);
        }
    }
    Html(app.tera.render("index.html", &context).unwrap())
}
