use crate::app::AppState;
use crate::models::{Post, Topic, User};
use axum::extract::{rejection::FormRejection, Path, State, Extension};
use axum::response::{Html, Response, Redirect, IntoResponse};
use tera::Context;
use serde::{Deserialize, Serialize};
use axum::{http::Method, Form};

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct CreatePostForm {
    pub title: String,
    pub body: String,
}

impl CreatePostForm {
    fn new() -> Self {
        Self {
            title: String::new(),
            body: String::new()
        }
    }
}

pub async fn create_post(
    State(app): State<AppState>,
    topic_slug: Path<String>,
    method: Method,
    Extension(user_id): Extension<i32>,
    raw_form: Result<Form<CreatePostForm>, FormRejection>,
) -> Response {
    let mut context = Context::new();
    // lookup topic
    let topic = match Topic::get(&app.db, topic_slug.0).await {
        Err(_) => return Html(app.tera.render("404.html", &context).unwrap()).into_response(),
        Ok(t) => match t {
          None => return Html(app.tera.render("404.html", &context).unwrap()).into_response(),
          Some(topic) => topic
        },
    };
    if method == "POST" {
        println!("post request");
        let form = match raw_form {
            Ok(f) => f,
            Err(e) => {
                context.insert("form", &CreatePostForm::new());
                context.insert("message", "Unknown server error");
                return Html(app.tera.render("posts/create.html", &context).unwrap()).into_response();
            }
        };
        // validate
        // save to db
        let post_form = form.0.clone();
        match Post::create(&app.db, post_form.title, post_form.body, user_id, topic.id).await {
            Err(_) => return Html(app.tera.render("posts/_detail.html", &context).unwrap()).into_response(),
            Ok(_) => return Redirect::to(&format!("/t/{}/", topic.slug.value)).into_response()
        }
    }

    let new_form = CreatePostForm::new();
    context.insert("form", &new_form);
    Html(app.tera.render("posts/create.html", &context).unwrap()).into_response()
}

pub async fn get_post(State(app): State<AppState>, slug: Path<String>) -> Html<String> {
    let mut context = Context::new();
    // check if htmx request
    match Post::lookup_slug(&app.db, slug.0).await {
        Err(_) => println!("error"),
        Ok(post) => {
            let user = User::lookup(&app.db, "id".into(), format!("{}", post.user_id)).await.expect("Missing user");
            let topic = Topic::lookup(&app.db, "id".into(), format!("{}", post.topic_id)).await.expect("Missing topic");
            context.insert("post", &post);
            context.insert("topic", &topic);
            context.insert("user", &user);
        }
    }
    Html(app.tera.render("posts/_detail.html", &context).unwrap())
}
