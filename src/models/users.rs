use argon2::{
    password_hash::{rand_core::OsRng, PasswordHasher, SaltString},
    Argon2,
};
use chrono::serde::ts_seconds;
use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use sqlx::postgres::Postgres;
use sqlx::Pool;

#[derive(Serialize, Deserialize, Debug)]
pub struct Email {
    pub value: String,
}

impl From<String> for Email {
    fn from(value: String) -> Self {
        Email { value }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Password {
    pub value: String,
}

impl From<String> for Password {
    fn from(value: String) -> Self {
        Password { value }
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Username {
    pub value: String,
}

impl From<String> for Username {
    fn from(value: String) -> Self {
        Username { value }
    }
}

impl PartialEq<&str> for Username {
    fn eq(&self, other: &&str) -> bool {
        self.value == other.to_string()
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct User {
    pub id: i32,
    pub email: Email,
    pub password: Password,
    pub username: Username,
    pub name: String,
    #[serde(with = "ts_seconds")]
    pub created: DateTime<Utc>,
    #[serde(with = "ts_seconds")]
    pub modified: DateTime<Utc>,
}

impl User {
    pub async fn create(
        pool: &Pool<Postgres>,
        email: String,
        password: String,
        username: String,
        name: String,
    ) -> Result<Self, sqlx::Error> {
        let salt = SaltString::generate(&mut OsRng);
        let argon2 = Argon2::default();
        let password_hash = argon2
            .hash_password(password.as_bytes(), &salt)
            .unwrap()
            .to_string();
        sqlx::query_as!(
            User,
            "
                INSERT INTO users(email, password, username, name)
                VALUES($1, $2, $3, $4)
                RETURNING *
            ",
            email,
            password_hash,
            username,
            name
        )
        .fetch_one(pool)
        .await
    }
    pub async fn lookup(
        pool: &Pool<Postgres>,
        field: String,
        value: String,
    ) -> Result<User, sqlx::Error> {
        let res: Result<User, sqlx::Error> = match field.as_ref() {
            "id" => {
                let id: i32 = value.parse().unwrap();
                sqlx::query_as!(User, "SELECT * FROM users WHERE id = $1", id)
                    .fetch_one(pool)
                    .await
            }
            "email" => {
                sqlx::query_as!(User, "SELECT * FROM users WHERE email = $1", value)
                    .fetch_one(pool)
                    .await
            }
            "username" => {
                sqlx::query_as!(User, "SELECT * FROM users WHERE username = $1", value)
                    .fetch_one(pool)
                    .await
            }
            _ => Err(sqlx::Error::ColumnNotFound(
                "Not a valid column name".into(),
            )),
        };
        res
    }
}

// impl User {
//     fn get_by_id() -> Self {}
//     fn get_by_email() -> Self {}
//     fn get_by_username() -> Self {}
//     fn create() -> Self {
//         // check email unique
//         // check password
//         // hash password
//         // save details to db, and return row as user instance
//     }
//     fn
// }
