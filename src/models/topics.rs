use crate::models::Slug;
use chrono::serde::ts_seconds;
use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use sqlx::{Pool, Postgres};

#[derive(Serialize, Deserialize)]
pub struct Topic {
    pub id: i32,
    pub name: String,
    pub slug: Slug,
    #[serde(with = "ts_seconds")]
    pub created: DateTime<Utc>,
    #[serde(with = "ts_seconds")]
    pub modified: DateTime<Utc>,
}

impl Topic {
    pub async fn get(pool: &Pool<Postgres>, slug: String) -> Result<Option<Topic>, sqlx::Error> {
        sqlx::query_as!(Topic, "SELECT * FROM topics WHERE slug = $1;", slug)
            .fetch_optional(pool)
            .await
    }
    pub async fn lookup(
        pool: &Pool<Postgres>,
        field: String,
        value: String,
    ) -> Result<Topic, sqlx::Error> {
        let res: Result<Topic, sqlx::Error> = match field.as_ref() {
            "id" => {
                let id: i32 = value.parse().unwrap();
                sqlx::query_as!(Topic, "SELECT * FROM topics WHERE id = $1", id)
                    .fetch_one(pool)
                    .await
            }
            "slug" => {
                sqlx::query_as!(Topic, "SELECT * FROM topics WHERE slug = $1", value)
                    .fetch_one(pool)
                    .await
            }
            _ => Err(sqlx::Error::ColumnNotFound(
                "Not a valid column name".into(),
            )),
        };
        res
    }
}
