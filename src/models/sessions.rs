use chrono::serde::ts_seconds;
use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use sqlx::postgres::Postgres;
use sqlx::Pool;
use uuid::Uuid;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Session {
    pub id: Uuid,
    pub user_id: i32,
    #[serde(with = "ts_seconds")]
    pub expires: DateTime<Utc>,
}

impl Session {
    pub async fn create(pool: &Pool<Postgres>, user_id: i32) -> Result<Self, sqlx::Error> {
        sqlx::query_as!(
            Self,
            "INSERT INTO sessions(user_id) VALUES($1) RETURNING * ",
            user_id
        )
        .fetch_one(pool)
        .await
    }
    pub async fn lookup(
        pool: &Pool<Postgres>,
        field: String,
        value: String,
    ) -> Result<Session, sqlx::Error> {
        let res: Result<Session, sqlx::Error> = match field.as_ref() {
            "id" => {
                // make into a uuid
                let id = match Uuid::parse_str(&value) {
                    Err(_) => {
                        return Err(sqlx::Error::ColumnNotFound(
                            "Not a valid column name".into(),
                        ))
                    }
                    Ok(id) => id,
                };
                sqlx::query_as!(Session, "SELECT * FROM sessions WHERE id = $1", id)
                    .fetch_one(pool)
                    .await
            }
            _ => Err(sqlx::Error::ColumnNotFound(
                "Not a valid column name".into(),
            )),
        };
        res
    }
}
