use crate::models::Slug;
use chrono::serde::ts_seconds;
use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use sqlx::postgres::Postgres;
use sqlx::Pool;
use slug::slugify;
use rand::{thread_rng, Rng};
use rand::distributions::Alphanumeric;

#[derive(Serialize, Deserialize)]
pub struct Post {
    pub id: i32,
    pub votes: i32,
    pub title: String,
    pub slug: Slug,
    pub body: String,
    pub user_id: i32,
    pub topic_id: i32,
    #[serde(with = "ts_seconds")]
    pub created: DateTime<Utc>,
    #[serde(with = "ts_seconds")]
    pub modified: DateTime<Utc>,
}

impl Post {
    pub async fn all(pool: &Pool<Postgres>) -> Result<Vec<Self>, sqlx::Error> {
        sqlx::query_as!(Post, "SELECT * FROM posts;")
            .fetch_all(pool)
            .await
    }
    pub async fn by_topic(pool: &Pool<Postgres>, topic_id: i32) -> Result<Vec<Self>, sqlx::Error> {
        sqlx::query_as!(Post, "SELECT * FROM posts WHERE topic_id = $1", topic_id)
            .fetch_all(pool)
            .await
    }
    pub async fn lookup_slug(pool: &Pool<Postgres>, slug: String) -> Result<Self, sqlx::Error> {
        sqlx::query_as!(Post, "SELECT * FROM posts WHERE slug = $1", slug)
            .fetch_one(pool)
            .await
    }
    pub async fn create(
        pool: &Pool<Postgres>,
        title: String,
        body: String,
        user_id: i32,
        topic_id: i32
    ) -> Result<Self, sqlx::Error> {
        let mut slug = slugify(title.clone());
        if Post::lookup_slug(pool, slug.clone()).await.is_ok() {
            let rand_string: String = thread_rng()
                .sample_iter(&Alphanumeric)
                .take(10)
                .map(char::from)
                .collect();
            slug = format!("{}-{}", slug, rand_string);
        }
        sqlx::query_as!(
            Post,
            "
                INSERT INTO posts(title, slug, body, user_id, topic_id)
                VALUES($1, $2, $3, $4, $5)
                RETURNING *
            ",
            title,
            slug,
            body,
            user_id,
            topic_id,
        )
        .fetch_one(pool)
        .await
    }
}
