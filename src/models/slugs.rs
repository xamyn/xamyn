use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct Slug {
    pub value: String,
}

impl From<String> for Slug {
    fn from(value: String) -> Self {
        Self { value }
    }
}
